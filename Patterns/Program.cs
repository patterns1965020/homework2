﻿
using System;
using System.Reflection;
using System.Reflection.Metadata;

public abstract class Product
{
    public int Cost { get; private set; }
    public string Description { get; private set; }

    public Product(int cost, string description)
    {
        Cost = cost;
        Description = description;
    }

    public abstract void Display();
}

public class BlackTea : Product
{
    string flavor;
    public BlackTea(string description, string flavor, int cost) : base(cost, description)
    {
        this.flavor = flavor;
    }

    public override void Display()
    {
        Console.WriteLine("\nBlack Tea ----- ");
        Console.WriteLine($" Description: {Description}");
        Console.WriteLine($" flavor: {flavor}");
        Console.WriteLine($" Cost: {Cost}");
    }
}

public class Coffee : Product
{
    string beansType;
    string releaseForm;
    string roasting;
    public Coffee(string description, string beansType, string releaseForm, string roasting, int cost) : base(cost, description)
    {
        this.beansType = beansType;
        this.releaseForm = releaseForm;
        this.roasting = roasting;
    }

    public override void Display()
    {
        Console.WriteLine("\nCoffee ----- ");
        Console.WriteLine(" Description: {0}", Description);
        Console.WriteLine(" Beans type: {0}", beansType);
        Console.WriteLine(" Release form: {0}", releaseForm);
        Console.WriteLine(" Roasting: {0}", roasting);
        Console.WriteLine(" Cost: {0}", Cost);
    }
}

public abstract class Decorator : Product
{
    protected Product product;

    public Decorator(Product product) : base(product.Cost, product.Description)
    {
        this.product = product;
    }

    public override void Display()
    {
        product.Display();
    }
}

public class DiscountedProduct : Decorator
{

    int discountCost;

    public DiscountedProduct(Product product, int sale)
        : base(product)
    {
        this.discountCost = product.Cost * (100 - sale) / 100;
    }


    public override void Display()
    {
        base.Display();

        Console.WriteLine(" Sale with discount: {0} ", discountCost);
    }
}

public class Shop
{
    List<Product> products = new List<Product>();
    List<IProductObserver> observers = new List<IProductObserver>();

    public void addProduct(Product product)
    {
        products.Add(product);
        NotifyObservers(product);
    }

    public void RegisterObserver(IProductObserver observer)
    {
        observers.Add(observer);
    }

    public void RemoveObserver(IProductObserver observer)
    {
        observers.Remove(observer);
    }

    private void NotifyObservers(Product product)
    {
        foreach (IProductObserver observer in observers)
        {
            observer.onProductAdded(product);
        }
    }
}

public interface IProductObserver
{
    void onProductAdded(Product product);
}

public class EventLog : IProductObserver
{
    private static EventLog log;

    private EventLog() { }
    public void onProductAdded(Product product)
    {
        Console.WriteLine($"Product {product.Description} added." );
    }

    public static EventLog getLog()
    {
        if (log == null) log = new EventLog();
        return log;
    }
}

public class Program
{
    public static void Main(string[] args)
    {

        Coffee coffee = new Coffee("Espresso Vending", "arabica + robusta", "beans", "medium", 400);
        coffee.Display();


        BlackTea blackTea = new BlackTea("Earl Grey", "without flavoring", 250);
        blackTea.Display();

        BlackTea keemunTea = new BlackTea("Keemun", "with flavoring", 300);
        keemunTea.Display();

        Console.WriteLine("\nSale:");

        DiscountedProduct discountBlackTea = new DiscountedProduct(blackTea, 15);
        discountBlackTea.Display();

        DiscountedProduct discountCoffee = new DiscountedProduct(coffee, 10);
        discountCoffee.Display();


        Console.WriteLine("\n----------------");

        Shop shop = new Shop();

        EventLog eventLog = EventLog.getLog();

        shop.RegisterObserver(eventLog);

        shop.addProduct(coffee);
        shop.addProduct(blackTea);
        shop.addProduct(keemunTea);

        Console.ReadKey();
    }
}

